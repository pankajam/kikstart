package lrucache;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

class LRUCache<T> {
    private LinkedList<String> lruList;
    private Map<String, T> cache;
    private Integer capacity;

    LRUCache() {
        this.lruList = new LinkedList<>();
        this.cache = new HashMap<>();
        this.capacity = 5;
    }

    LRUCache(Integer capacity) {
        this.lruList = new LinkedList<>();
        this.cache = new HashMap<>();
        this.capacity = capacity;
    }

    void set(String key, T value) {
        if (cache.size() >= capacity) {
            String last = lruList.remove();
            cache.remove(last);
            cache.put(key, value);
            lruList.add(key);
        } else {
            cache.put(key, value);
            lruList.add(key);
        }
    }

    T get(String key) {
        if (cache.get(key) != null) {
            lruList.remove(key);
            lruList.add(key);
            return cache.get(key);
        }
        return (T) "Key not found!";
    }

    // Test
    public static void main(String[] args) {
        LRUCache lru = new LRUCache();
        lru.set("foo", "bar");
        lru.set("key1", 1);
        lru.set("key2", 3.1);
        lru.set("key3", 3);
        lru.set("key4", 4);
        System.out.println(lru.get("foo"));
        System.out.println(lru.get("key1"));
        System.out.println(lru.get("key2"));
        System.out.println(lru.get("key3"));
        System.out.println(lru.get("key4"));
        lru.set("key5", 5);
        lru.set("key6", 6);
        System.out.println(lru.get("foo"));
        System.out.println(lru.get("key5"));
        System.out.println(lru.get("key1"));
    }
}