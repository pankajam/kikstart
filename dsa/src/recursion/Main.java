package recursion;

public class Main {
    // single recursion
    // multiple recursion
    // corecursion
    // Indirect recursion
    // Anonymous recursion
    // Structural versus generative recursion
    // https://en.wikipedia.org/wiki/Recursion_(computer_science)
    // https://en.wikipedia.org/wiki/Corecursion#Examples
    // https://en.wikipedia.org/wiki/How_to_Design_Programs
    // https://en.wikipedia.org/wiki/Threaded_binary_tree

    public static void main(String[] args) {
        System.out.println(factorial(5));
        System.out.println(gcd(4, 2));
        System.out.print(towersOfHanoi(5));
    }

    static int factorial(int n) {
        if (n == 0) {
            return 1;
        }
        return n * factorial(n-1);
    }

    // tail recursion
    static int gcd(int x, int y) {
        if (y == 0) {
            return x;
        } else {
            return gcd(y, x % y);
        }
    }

    static int towersOfHanoi(int n) {
        if (n == 1) {
            return 1;
        }
        return 2 * towersOfHanoi(n-1) + 1;
    }

}
