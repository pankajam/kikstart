package recursion;

import java.io.File;

public class FileSystem {
    static void traverse() {
        File[] fs = File.listRoots();
        for (File f : fs) {
            if (f.isDirectory() && f.canRead()) {
                rtraverse(f);
            }
        }
    }

    static void rtraverse(File fd) {
        File[] fss = fd.listFiles();
        for (File file : fss) {
            System.out.println(file);
            if (file.isDirectory() && file.canRead()) {
                rtraverse(file);
            }
        }


    }

    public static void main(String[] args) {
        traverse();
    }
}
