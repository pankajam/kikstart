package stack;



public class Trie {
    private static final int ALPHABET_SIZE = 26;

    static class TrieNode {
        TrieNode[] child = new TrieNode[ALPHABET_SIZE];
        boolean isEndOfWord;

        TrieNode() {
            for (int i=0; i<ALPHABET_SIZE; i++) {
                child[i] = null;
            }
            isEndOfWord = false;
        }
    }

    static void insert(TrieNode root, String key) {
        TrieNode temp = root;
        for (int level=0; level<key.length(); level++) {
            int index = key.charAt(level) - 'a';
            if (temp.child[index] == null) {
                temp.child[index] = new TrieNode();
            }
            temp = temp.child[index];
        }
        temp.isEndOfWord = true;
    }

    static void printTrie(TrieNode root) {
        TrieNode temp = root;
        if (temp == null) {
            return;
        }
        for (int i=0; i<ALPHABET_SIZE; i++) {
             if (temp.child[i] != null){
                 System.out.print(i + " ");
                printTrie(temp.child[i]);
            }
        }
    }

    public static void main(String[] args) {
        TrieNode root = new TrieNode();
        String keys[] = {"the", "a", "there", "answer", "any",
            "by", "bye", "their"};
        int i;
        for (i = 0; i < keys.length ; i++)
            insert(root, keys[i]);
        printTrie(root);
    }




}
