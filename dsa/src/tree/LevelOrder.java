package tree;

public class LevelOrder {
    static void printLevelOrder(Node<Integer> root) {
        int h = Height.findHeight(root);
        for (int i=1; i<=h; i++) {
            printLevelOrderUtil(root, i);
        }
    }

    static void printLevelOrderUtil(Node<Integer> root, int level) {
        if (level == 1) {
            System.out.print(root.value + " ");
        } else {
            printLevelOrderUtil(root.left, level-1);
            printLevelOrderUtil(root.right, level-1);
        }
    }

    public static void main(String[] args) {
        Node<Integer> root = new Node<>(1);
        root.left = new Node<>(2);
        root.right = new Node<>(3);
        root.left.left = new Node<>(7);
        root.left.right = new Node<>(6);
        root.right.left = new Node<>(5);
        root.right.right = new Node<>(4);

        printLevelOrder(root);
    }
}
