package tree;

public class Node<T> {
    public Node left;
    public Node right;
    public T value;

    public Node(T value) {
        this.left = null;
        this.right = null;
        this.value = value;
    }

    static void printPreorder(Node root) {
        if (root == null) return;
        System.out.print(root.value + " ");
        printPreorder(root.left);
        printPreorder(root.right);
    }
}
