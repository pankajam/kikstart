package tree.binaryTree;

import tree.Node;

public class SameTree {
    static boolean isSameTree(Node<Integer> root1, Node<Integer> root2) {
        if (root1 == null && root2 == null) return true;
        return (root1.value == root2.value
            && isSameTree(root1.left, root2.left)
            && isSameTree(root1.right, root2.right));
    }

    public static void main(String[] args) {
        Node<Integer> root1 = new Node<>(1);
        root1.left = new Node<>(2);
        root1.right = new Node<>(3);
        root1.left.left = new Node<>(7);
        root1.left.right = new Node<>(6);
        root1.right.left = new Node<>(5);
        root1.right.right = new Node<>(4);

        Node<Integer> root2 = new Node<>(1);
        root2.left = new Node<>(2);
        root2.right = new Node<>(3);
        root2.left.left = new Node<>(7);
        root2.left.right = new Node<>(6);
        root2.right.left = new Node<>(5);
        root2.right.right = new Node<>(4);
        System.out.print(isSameTree(root1, root2));
    }
}
