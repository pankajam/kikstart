package tree.binaryTree;

import tree.Node;

public class Height {
    static Integer calculateHight(Node<Integer> root) {
        if (root == null) return 0;

        int leftHeight = calculateHight(root.left);
        int rightHeight = calculateHight(root.right);

        return 1 + Math.max(leftHeight, rightHeight);
    }

    public static void main(String[] args) {
        Node<Integer> root = new Node<>(1);
        root.left = new Node<>(2);
        root.right = new Node<>(3);
        root.left.left = new Node<>(7);
        root.left.right = new Node<>(6);
        root.right.left = new Node<>(5);
        root.right.right = new Node<>(4);
        System.out.print(calculateHight(root));
    }
}
