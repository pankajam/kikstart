package tree.binaryTree;

import tree.Node;

import java.util.LinkedList;

public class RootToLeafSum {
    static boolean findPathSum(Node<Integer> root, int sum, LinkedList<Integer> result) {
        if (root.left == null && root.right == null) {
            if (root.value == sum) {
                result.add(root.value);
                return true;
            } else {
                return false;
            }
        }
        assert root.left != null;
        if (findPathSum(root.left, sum-root.value, result)) {
            result.add(root.value);
            return true;
        }
        assert root.right != null;
        if (findPathSum(root.right, sum-root.value, result)) {
            result.add(root.value);
            return true;
        }
        return false;
    }

    static LinkedList<Integer> findPathSumUtil(Node<Integer> root, int sum) {
        LinkedList<Integer> result = new LinkedList<>();
        findPathSum(root, sum, result);
        return result;
    }

    public static void main(String[] args) {
        Node<Integer> root = new Node<>(1);
        root.left = new Node<>(2);
        root.right = new Node<>(3);
        root.left.left = new Node<>(7);
        root.left.right = new Node<>(6);
        root.right.left = new Node<>(5);
        root.right.right = new Node<>(4);
        LinkedList<Integer> result = findPathSumUtil(root, 9);
        for (Integer val: result) {
            System.out.print(val + " ");
        }
    }
}
