package tree.binaryTree;

import tree.Node;

public class LCA {
    static Node findLCA(Node<Integer> root, int n1, int n2) {
        if (root == null) return null;
        if (root.value == n1 || root.value == n2) return root;

        Node leftNode = findLCA(root.left, n1, n2);
        Node rightNode = findLCA(root.right, n1, n2);

        if (leftNode != null && rightNode != null) return root;
        if (leftNode == null && rightNode == null) return null;

        return leftNode != null?leftNode:rightNode;
    }

    public static void main(String[] args) {
        Node<Integer> root = new Node<>(1);
        root.left = new Node<>(2);
        root.right = new Node<>(3);
        root.left.left = new Node<>(7);
        root.left.right = new Node<>(6);
        root.right.left = new Node<>(5);
        root.right.right = new Node<>(4);
        System.out.print(findLCA(root, 7, 4).value);
    }
}
