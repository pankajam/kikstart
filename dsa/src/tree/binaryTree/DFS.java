package com.company.dsa.tree.binaryTree;

import com.company.dsa.tree.Node;

public class DFS {
    static void preOrder(Node root) {
        if (root == null) return;
        System.out.print(root.value + " ");
        preOrder(root.left);
        preOrder(root.right);
    }

    static void inOrder() {

    }

    static void postOrder() {

    }

    public static void main(String[] args) {
        //       1
        //      / \
        //     2   3
        //   / \  / \
        //  7  6 5  4
        Node<Integer> root = new Node<>(1);
        root.left = new Node<>(2);
        root.right = new Node<>(3);
        root.left.left = new Node<>(7);
        root.left.right = new Node<>(6);
        root.right.left = new Node<>(5);
        root.right.right = new Node<>(4);
        preOrder(root);
    }
}
