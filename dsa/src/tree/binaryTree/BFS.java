package tree.binaryTree;

import tree.Node;

import java.util.LinkedList;
import java.util.Queue;

public class BFS {
    static void levelOrder(Node<Integer> root) {
        Queue<Node> queue = new LinkedList<>();
        queue.add(root);
        while (!queue.isEmpty()) {
            Node temp = queue.poll();
            System.out.print(temp.value + " ");
            if (temp.left != null) {
                queue.add(temp.left);
            }
            if (temp.right != null) {
                queue.add(temp.right);
            }
        }
    }

    public static void main(String[] args) {
        Node<Integer> root = new Node<>(1);
        root.left = new Node<>(2);
        root.right = new Node<>(3);
        root.left.left = new Node<>(7);
        root.left.right = new Node<>(6);
        root.right.left = new Node<>(5);
        root.right.right = new Node<>(4);
        levelOrder(root);
    }
}
