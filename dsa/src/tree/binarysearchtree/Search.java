package tree.binarysearchtree;

import tree.Node;

public class Search {
    static Node search(Node<Integer> root, Integer key) {
        if (root == null) return null;
        if (root.value.equals(key)) return root;
        else if (root.value < key) return search(root.right, key);
        else return search(root.left, key);
    }

    public static void main(String[] args) {
        Node<Integer> root = new Node<>(10);
        root.left = new Node<>(-5);
        root.right = new Node<>(16);
        root.left.left = new Node<>(-8);
        root.left.right = new Node<>(7);
        root.right.right = new Node<>(18);
        System.out.println(search(root, 10));
    }
}
