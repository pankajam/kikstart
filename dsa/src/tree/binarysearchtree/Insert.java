package tree.binarysearchtree;

import tree.Node;

public class Insert {
    static Node insert(Node<Integer> root, Integer data) {
        Node newNode = new Node(data);
        if (root == null) return newNode;
        Node<Integer> current = root;
        Node<Integer> prv = null;
        while (current != null) {
            prv = current;
            if (current.value < data) {
                current = current.right;
            } else {
                current = current.left;
            }
        }
        if (prv.value > data) {
            prv.left = newNode;
        } else {
            prv.right = newNode;
        }
        return root;
    }

    static void print(Node root) {
        if (root == null) return;
        print(root.left);
        System.out.print(root.value + " ");
        print(root.right);
    }

    public static void main(String[] args) {
        Node<Integer> root = new Node<>(10);
        root.left = new Node<>(-5);
        root.right = new Node<>(16);
        root.left.left = new Node<>(-8);
        root.left.right = new Node<>(7);
        root.right.right = new Node<>(18);
        print(root);
        root = insert(root, 6);
        System.out.println();
        print(root);
    }
}
