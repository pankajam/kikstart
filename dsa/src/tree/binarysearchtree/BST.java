package tree.binarysearchtree;

import tree.Node;

public class BST {
    static boolean isBSTUtil(Node<Integer> root, int min, int max) {
       if (root == null) return true;
        if (root.value < min || root.value > max) return false;

        return isBSTUtil(root.left, min, root.value) && isBSTUtil(root.right, root.value, max);
    }

    static boolean isBST(Node<Integer> root) {
        return isBSTUtil(root, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    public static void main(String[] args) {
        Node<Integer> root = new Node<>(1);
        root.left = new Node<>(2);
        root.right = new Node<>(3);
        root.left.left = new Node<>(7);
        root.left.right = new Node<>(6);
        root.right.left = new Node<>(5);
        root.right.right = new Node<>(4);
        System.out.println(isBST(root));

        Node<Integer> root1 = new Node<>(10);
        root1.left = new Node<>(-5);
        root1.right = new Node<>(16);
        root1.left.left = new Node<>(-8);
        root1.left.right = new Node<>(7);
        root1.right.right = new Node<>(18);
        System.out.println(isBST(root1));
    }
}
