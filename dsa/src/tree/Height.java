package tree;

public class Height {
    static int findHeight(Node<Integer> root) {
        if (root == null) return 0;

        else {
            int leftHeight = findHeight(root.left);
            int rightHeight = findHeight(root.right);

            return  (leftHeight > rightHeight)?leftHeight+1:rightHeight+1;
        }
    }

    public static void main(String[] args) {
        Node<Integer> root = new Node<>(1);
        root.left = new Node<>(2);
        root.right = new Node<>(3);
        root.left.left = new Node<>(7);
        root.left.right = new Node<>(6);
        root.right.left = new Node<>(5);
        root.right.right = new Node<>(4);

        System.out.println("Height of the tree is "+ findHeight(root));
    }
}
