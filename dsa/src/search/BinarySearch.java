package search;

public class BinarySearch {
    // iterative
    static int search(int[] arr, int k) {
        int l = 0;
        int r = arr.length - 1;
        while (l <= r) {
            int m = l + (r-l)/2;
            if (arr[m] < k) {
                l = m + 1;
            } else if (arr[m] == k) {
                return m;
            } else {
                r = m - 1;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};
        assert search(arr, 1) == 0 : "FAIL";
        assert search(arr, 2) == 1 : "FAIL";
        assert search(arr, 3) == 2 : "FAIL";
        assert search(arr, 4) == 3 : "FAIL";
        assert search(arr, 5) == 4 : "FAIL";
        assert search(arr, 6) == -1 : "FAIL";
    }
}
