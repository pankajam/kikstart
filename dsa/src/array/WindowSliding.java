package array;

public class WindowSliding {
    private static int maxSum(int[] arr, int n, int k) {
        if ( k > n) {
            return -1;
        }
        int max = 0;
        for (int i = 0; i < k; i++) {
            max += arr[i];
        }
        int windowSum = max;
        for (int i = k; i < n; i++) {
            windowSum += arr[i] - arr[i-k];
            if (max < windowSum) {
                max = windowSum;
            }
        }
        return max;
    }

    public static void main(String[] args) {
        int arr[] = { 1, 4, 2, 10, 2, 3, 1, 0, 20 };
        int k = 4;
        int n = arr.length;
        System.out.println(maxSum(arr, n, k));
    }
}
