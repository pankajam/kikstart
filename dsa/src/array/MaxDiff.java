package array;// Find max diff between two array element a[j]-a[i]; j>i

public class MaxDiff {
    public static void maxDiff(int[] arr) {
        int n = arr.length;
        int[] maxArr = new int[n];
        maxArr[n-1] = 0;
        int maxSoFar =  arr[n-1];
        for (int i = n-1; i > 0; i--) {
            if (maxSoFar < arr[i]) {
                maxArr[i-1] = arr[i];
                maxSoFar = arr[i];
            } else {
                maxArr[i-1] = maxSoFar;
            }
        }
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < n; i++) {
            int diff = maxArr[i]- arr[i];
            if (diff > max) {
                max = diff;
            }
        }
        System.out.println(max);
    }

    public static void main(String[] args) {
        int[] arr = {2, 3, 10, 6, 4, 8, 1};
        maxDiff(arr);
    }
}
