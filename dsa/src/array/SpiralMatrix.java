package array;

public class SpiralMatrix {
    private static void printMatrix(int[][] arr, int left, int right, int top, int bottom) {
        while ((left <= right) && (top <= bottom)) {
            for (int i = left; i <= right; i++) {
                System.out.print(arr[top][i] + " ");
            }
            top++;
            for (int i = top; i <= bottom; i++) {
                System.out.print(arr[i][right] + " ");
            }
            right--;
            for (int i = right; i >= left; i--) {
                System.out.print(arr[bottom][i] + " ");
            }
            bottom--;
            for (int i = bottom; i >= top; i--) {
                System.out.print(arr[i][left] + " ");
            }
            left++;
        }
    }
    public static void main(String[] args) {
        int[][] arr = { { 1, 2, 3, 4 },
                        { 5, 6, 7, 8 },
                        { 9, 10, 11, 12 },
                        { 13, 14, 15, 16 } };
        int left = 0;
        int top = 0;
        int right = 3;
        int bottom = 3;
        printMatrix(arr, left, right, top, bottom);
    }
}
