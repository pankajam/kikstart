package graph;

import java.util.ArrayList;

class UndirectedGraph {
    private ArrayList<ArrayList<Integer>> adj;

    UndirectedGraph(Integer V) {
        this.adj = new ArrayList<>(V);
        for (int i=0; i<V; i++) {
            adj.add(new ArrayList<>());
        }
    }

    void addEdge(Integer u, Integer v) {
        adj.get(u).add(v);
        adj.get(v).add(u);
    }

    void printNode() {
        for (int i=0; i<adj.size(); i++) {
            System.out.print("head" + i);
            for (int j = 0; j < adj.get(i).size(); j++) {
                System.out.print("->" + adj.get(i).get(j));
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {

    }
}

public class Graph {
    public static void main(String[] args) {
        UndirectedGraph ug = new UndirectedGraph(5);
        ug.addEdge(0, 1);
        ug.addEdge(0, 2);
        ug.addEdge(1, 3);
        ug.addEdge(3, 4);
        ug.addEdge(2, 4);
        ug.printNode();
    }
}
