package graph;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Queue;

public class BFS {

    class Graph {
        private ArrayList<ArrayList<Integer>> adj;
        Graph(Integer V) {
            for (int i=0; i<V; i++) {
                adj.add(new ArrayList<>());
            }
        }
    }

    void breathFirstSearch(Integer v, ArrayList<ArrayList<Integer>> adj) {
        if (v==0) return;
        //boolean
        boolean[] visited = new boolean[v];
        for (int i=0; i<v; i++) visited[i] = false;
        visited[0] = true;
        Queue<Integer> queue = new ArrayDeque<>();
        queue.add(0);
        while (!queue.isEmpty()) {
            Integer temp = queue.poll();
            System.out.print(temp + " ");
            ArrayList<Integer> list = adj.get(temp);
            for (Integer node: list) {
                if (!visited[node]) {
                    queue.add(node);
                    visited[node] = true;
                }
            }
        }
    }

    public static void main(String[] args) {

    }
}
