package heap;

public class MaxPQ <Key extends Comparable<Key>> {
    private Key[] pq;
    private int N = 0;

    public MaxPQ(int cap) {
        pq = (Key[]) new Comparable[cap + 1];
    }

    public Key max() {
        return pq[1];
    }

    public void insert(Key e) {
        N++;
        pq[N] = e;
        swim(N);
    }

    public Key deleteMax() {
        Key max = pq[1];
        exch(1, N);
        pq[N] = null;
        N--;
        sink(1);
        return max;
    }

    private void swim(int k) {
        while (k > 1 && less(parent(k), k)) {
            exch(parent(k), k);
            k = parent(k);
        }
    }

    private void sink(int k) {
        while (left(k) <= N) {
            int older = left(k);
            // if right exist and less than left
            if (right(k) <= N && less(older, right(k))) {
                older = right(k);
            }

            if (less(older, k)) break;

            exch(older, k);
            k = older;

        }
    }

    private void exch(int i, int j) {
        Key temp = pq[i];
        pq[i] = pq[j];
        pq[j] = temp;
    }

    private boolean less(int i, int j) {
        return pq[i].compareTo(pq[j]) < 0;
    }

    private int parent(int root) {
        return root / 2;
    }

    private int left(int root) {
        return root * 2;
    }

    private int right(int root) {
        return root * 2 + 1;
    }

    void display() {
        for (int i=1; i<=N; i++) {
            System.out.print(pq[i] + " ");
        }
        System.out.println();
    }
}
