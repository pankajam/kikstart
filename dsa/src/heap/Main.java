package heap;

public class Main {
    /*
    * Binary heap is a special kind of binary tree (complete binary tree) that is stored in an array
    * There are two main applications, the first is a sorting method "heap sort",
    * the second is a very useful data structure "priority queue"
    * */

    public static void main(String[] args) {
        // MaxPQ
        MaxPQ<Integer> pq = new MaxPQ<>(5);
        pq.insert(5);
        pq.insert(7);
        pq.insert(1);
        pq.insert(9);
        pq.insert(6);

        pq.display();

        pq.deleteMax();
        pq.deleteMax();
        pq.deleteMax();
        pq.deleteMax();
        pq.deleteMax();

        pq.display();

        // heap sort
        MaxPQ<Integer> pq1 = new MaxPQ<>(5);
        pq.insert(5);
        pq.insert(7);
        pq.insert(1);
        pq.insert(9);
        pq.insert(6);
    }

}
